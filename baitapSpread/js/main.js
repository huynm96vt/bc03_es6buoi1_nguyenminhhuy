let hoverEffect = () => {
    let content = document.getElementById("heading").textContent;
    let chars = [...content]
    let contentHTML = ``
    chars.forEach((item) => {
        let contentSpanTag = `
        <span>${item}</span>
        `
        contentHTML += contentSpanTag
    })

    document.getElementById("heading").innerHTML = contentHTML;
}

hoverEffect()