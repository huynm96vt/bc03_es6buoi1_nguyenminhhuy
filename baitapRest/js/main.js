let tinhDiemTB = (...thongTin) => {
  let tong = 0;
  thongTin.forEach((diem) => {
    tong += diem;
  });
  return  tong / (thongTin.length);
  
};

let diemTBKhoi1 = () => {
  let toan = document.getElementById("inpToan").value * 1;
  let ly = document.getElementById("inpLy").value * 1;
  let hoa = document.getElementById("inpHoa").value * 1;
  
  document.getElementById("tbKhoi1").innerText = tinhDiemTB(toan, ly, hoa);
};

document.getElementById("btnKhoi1").addEventListener("click", diemTBKhoi1);

let diemTBKhoi2 = () => {
    let van = document.getElementById("inpVan").value * 1;
  let su = document.getElementById("inpSu").value * 1;
  let dia = document.getElementById("inpDia").value * 1;
  let english = document.getElementById("inpEnglish").value * 1;
  document.getElementById("tbKhoi2").innerText = tinhDiemTB(van, su, dia,english);
}

document.getElementById("btnKhoi2").addEventListener("click", diemTBKhoi2);