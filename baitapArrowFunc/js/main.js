const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];

let renderColor = () => {
  let contentHtml = ``;
  colorList.forEach((item) => {
    let contentButton = `
        <button class = "color-button ${item}"></button>
        `;
    if(item == "pallet"){
        contentButton = `
        <button class = "color-button ${item} active"></button>
        `;
    }
    contentHtml += contentButton;
  });

  document.getElementById("colorContainer").innerHTML = contentHtml;
};


renderColor();

// Add active class to the current button 
let colors = document.getElementById("colorContainer");
let btns = colors.getElementsByClassName("color-button");
for (let i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
      let current = document.getElementsByClassName("active");
      current[0].className = current[0].className.replace("active", "");
      this.className += " active";
      let colorSelected = colorList[i];
      colorList.forEach((color) =>{
        document.querySelector(".house").classList.remove(`${color}`)
      })
      document.querySelector(".house").classList.add(`${colorSelected}`)
  });
}








